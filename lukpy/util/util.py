import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os

def multipage(filename, figs=None, dpi=300, png=False):
    ''' Saves all open (or list of) figures to filename.pdf with dpi''' 
    pp = PdfPages(filename)
    path = os.path.dirname(filename)
    fn = os.path.basename(filename)[:-4]

    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for i, fig in enumerate(figs):
        print(f'saving fig {fig}\n')
        fig.savefig(pp, format='pdf', dpi=dpi)
        if png:
            fig.savefig(f'{path}\\{i}_{fn}.png', dpi=600)
    pp.close()

def print_dict(dct):
    strings = ['Stats\n']
    for key, val in dct.items():
        try:
            keyvalpair = f'{key}: {val:.4}'
        except:
            keyvalpair = f'{key}: {val}'
        print(keyvalpair)
        strings.append(keyvalpair+'\n')
    return ''.join(strings)

def print_omni_dict(dct):
    strings = ['Stats\n']
    for key, val in dct.items():
        if val != '':
            try:
                keyvalpair = f'{key}: {val:.4}'
            except:
                keyvalpair = f'{key}: {val}'
            print(keyvalpair)
            strings.append(keyvalpair+'\n')
        else:
            strings.append('\n')
    return ''.join(strings)