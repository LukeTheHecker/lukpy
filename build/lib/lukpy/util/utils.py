import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os

def multipage(filename, figs=None, dpi=300):
    ''' Saves all open (or list of) figures to filename.pdf with dpi''' 
    pp = PdfPages(filename)
    path = os.path.dirname(filename)
    fn = os.path.basename(filename)[:-4]

    fn_png = filename[:-4] + '.png'
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for i, fig in enumerate(figs):
        print(f'saving fig {fig}\n')
        fig.savefig(pp, format='pdf')
        fig.savefig(f'{path}\\{i}_{fn}.png', dpi=600)
    pp.close()