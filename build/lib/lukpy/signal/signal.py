import numpy as np
import scipy
def movmean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)

def tolerant_mean(arrs, method='tol'):
    ''' method performs mean across traces with differnt number of points based on either of two methods:
    method = lcd -> lowest common denominator, i.e. mean is calculated across the lowest number of points
    method = tol -> tolerant mean, i.e. mean across the highest number of points
    '''
    if method=='tol':
        lens = [len(i) for i in arrs]
        arr = np.ma.empty((np.max(lens),len(arrs)))
        arr.mask = True
        for idx, l in enumerate(arrs):
            arr[:len(l),idx] = l
        return arr.mean(axis = -1), arr.std(axis=-1)
    elif method == 'lcd':
        min_len = np.min([len(i) for i in arrs])
        pruned_array = np.zeros((len(arrs), min_len,))
        for i, arr in enumerate(arrs):
            pruned_array[i,:] = arr[:min_len]
        return np.mean(pruned_array, axis=0), np.std(pruned_array, axis=0)

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def skew_norm_pdf(x, mu=0, w=1, alpha_skew=0):
    # adapated from:
    # http://stackoverflow.com/questions/5884768/skew-normal-distribution-in-scipy
    t = (x-mu) / w
    return 2.0 * w * scipy.stats.norm.pdf(t) * scipy.stats.norm.cdf(alpha_skew*t)