import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy.stats import ranksums, ttest_rel, ttest_ind
import numpy as np

from lukpy.signal import *
from lukpy.stats import *
from lukpy.util import *

def scatter_plot(x1, x2, title='', stat='perm', paired=False, n_perm=int(1e6), legend=('Group A', 'Group B'), plot_stat_summary=True, effort=1):
    ''' This function takes two vectors and plots them as scatter 
    plot and adds statistics to title'''

    if len(x1) > 0:
        if not x1[0]:
            return
    elif not x1:
        return

    if type(x1) == list:
        x1 = np.asarray(x1)
    if type(x2) == list:
        x2 = np.asarray(x2)

    val = [-2, 2]
    pos_x1 = val[0] + (np.random.rand(x1.size) * 2 - 1) * 0.3
    pos_x2 = val[1] + (np.random.rand(x2.size) * 2 - 1) * 0.3
    if stat=='wilc':
        t, p = ranksums(x1, x2)
    elif stat=='ttest':
        if paired:
            t, p = ttest_rel(x1, x2)
        else:
            t, p = ttest_ind(x1, x2)
    elif stat == 'perm':
        p = permutation_test(x1, x2, paired=False, tails=2, plot_me=False, n_perm=n_perm)
        t = 0
    
    plt.figure()
    colors = ['#0066ff', '#ff9900']
    plt.plot(pos_x1, x1, 'o', color=colors[0], markeredgecolor='black', label=legend[0])
    bbox_props = dict(facecolor=colors[0], alpha=0.6)
    plt.boxplot(x1, positions=[-2], patch_artist=True, notch=False, bootstrap=1000, whis=[5, 95], widths=0.5, boxprops=bbox_props, capprops=dict(color='black'), medianprops=dict(color='black', linewidth=2), showfliers=False)


    plt.plot(pos_x2, x2, 'o', color=colors[1], markeredgecolor='black', label=legend[1])
    bbox_props = dict(facecolor=colors[1], alpha=0.6)
    plt.boxplot(x2, positions=[2], patch_artist=True, notch=False, bootstrap=1000, whis=[5, 95], widths=0.5, boxprops=bbox_props, capprops=dict(color='black'), medianprops=dict(color='black', linewidth=2), showfliers=False)

    first_patch = mpatches.Patch(color=colors[0], label=legend[0])
    second_patch = mpatches.Patch(color=colors[1], label=legend[1])
    plt.legend(handles=[first_patch, second_patch])


    plt.title(f'{title}')
    
    if plot_stat_summary:
        om = omni_stat(x1, x2, paired=False, tails=2, verbose=0, effort=effort)
        # print('print_dict:\n')
        txt = print_omni_dict(om)
        # print('txt:\n')
        # print(txt)
        props = dict(boxstyle='round', facecolor='white', alpha=0.5)
        pos_x = -5.9
        pos_y = np.max(np.concatenate((x1, x2), axis=0))
        plt.text(pos_x, pos_y, txt, fontsize=8, verticalalignment='top', bbox=props)
        plt.xlim((-6, 6))
    else:
        plt.xlim((-5, 5))

    plt.show()

def plot_two_with_error(X, Y1, Y2, measure='SEM', labels=['Group A', 'Group B'], title='', ylabel='', xlabel='', xticklabels=None, test='perm'):
    '''
    Plots two Grand mean traces with underlying error shading
    X: Time array
    Y1: Group of observations 1
    Y2: Group of observations 2
    measure: ['SEM', 'SD'] Choose standard error of the mean (SEM) or standard deviation (SD)
    test: ['perm', 'wilc', 'ttest']
    '''
    if type(Y1) == list:
        Y1 = np.squeeze(np.array(Y1))
        Y2 = np.squeeze(np.array(Y2))

    m_y1 = np.mean(Y1, axis=0)
    sd_y1 = np.std(Y1, axis=0)

    m_y2 = np.mean(Y2, axis=0)
    sd_y2 = np.std(Y2, axis=0)
    if measure=='SEM':
        sd_y1 = np.array(sd_y1)
        sd_y1 /= np.sqrt(len(Y1))
        sd_y2 = np.array(sd_y2)
        sd_y2 /= np.sqrt(len(Y2))
    elif measure=='SD' or measure=='STD':
        print('')
    else:
        print(f'measure {measure} not available. Choose SEM or SD.')
        return
    plt.figure()
    plt.subplot(211)
    # Plot Y1
    plt.plot(X, m_y1, label=labels[0])
    plt.fill_between(X, m_y1-sd_y1, m_y1+sd_y1, alpha=0.3)
    # Plot Y2
    plt.plot(X, m_y2, label=labels[1])
    plt.fill_between(X, m_y2-sd_y2, m_y2+sd_y2, alpha=0.3)
    plt.legend()
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    if xticklabels != None:
        plt.xticks(X, xticklabels)
    plt.title(title)
    # P-values
    plt.subplot(212)
    p_vals = np.zeros_like(m_y1)
    for i in range(len(p_vals)):
        if test == 'perm':
            p_vals[i] = permutation_test(Y1[:, i], Y2[:, i])
        if test == 'wilc':
            p_vals[i] = ranksums(Y1[:, i], Y2[:, i])[1]
        if test == 'ttest':
            p_vals[i] = ttest_ind(Y1[:, i], Y2[:, i])[1]
    plt.plot(X, p_vals)
    plt.plot(X, np.ones_like(X)*0.05, '--')
    plt.yscale('log')
    plt.title(f'{test} p-value')
    if xticklabels != None:
        plt.xticks(X, xticklabels)
    plt.tight_layout(pad=2)
