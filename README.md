# Lukpy
Some helpful functions, shared for everyone.

## Installation

```
pip install git+https://bitbucket.org/LukeTheHecker/lukpy.git
```

## Test installation

```
from lukpy.stats import permutation_test
p = permutation_test([5, 2, 4, 1, 4], [8, 7, 5, 7, 8, 5, 7])
print(p)
```
