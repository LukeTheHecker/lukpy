import numpy as np
import matplotlib.pyplot as plt
from lukpy.stats import permutation_test, classification_test, omni_stat, decision_tree
from lukpy.signal import movmean, skew_norm_pdf, gaussian
from lukpy.plot import scatter_plot
from scipy.stats import wilcoxon, ranksums, ttest_rel, ttest_ind, normaltest, bartlett
# print(permutation_test([1, 2, 3], [3, 4, 5]))
# print(classification_test([1, 2, 3], [3, 4, 5]))
# print(movmean([1, 2, 3, 4, 5], 2))
omni_stat(np.random.randn(25)*2, np.random.rand(25) -0.1, verbose=1)
# x = np.linspace(-5, 5, 25)

# scatter_plot(np.random.normal(loc=0, scale=1, size=52), np.random.normal(loc=0.2, scale=0.5, size=52))

# t, p = bartlett(np.random.normal(loc=0, scale=1, size=25), np.random.normal(loc=0, scale=0.5, size=25))
# print(f't={t}, p={p}')

# plt.figure()
# plt.hist(np.random.normal(loc=0, scale=1, size=555), color='blue', alpha=0.3)
# plt.hist(np.random.normal(loc=0.1, scale=0.5, size=555), color='darkorange', alpha=0.3)
# plt.show()

# equal = True
# normal = False
# dec = decision_tree(equal, normal)
